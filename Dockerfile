FROM arm64v8/wordpress:php8.1-apache

COPY . .

ADD https://github.com/mlocati/docker-php-extension-installer/releases/latest/download/install-php-extensions /usr/local/bin/

RUN chmod +x /usr/local/bin/install-php-extensions && \
    install-php-extensions ioncube_loader;

# RUN echo "Prepare Entrypoint ..." \
#     # Prepare Entrypoint \
#     && ls -al \
#     && mv ./php/conf.d/* /usr/local/etc/php/conf.d/ \
#     && mv ./php/php-fpm.conf /usr/local/etc/php-fpm.d/www.conf \
#     && mv ./php/error-logging.ini /usr/local/etc/php/conf.d/error-logging.ini


# CMD ["php-fpm"]
CMD ["apache2-foreground"]
